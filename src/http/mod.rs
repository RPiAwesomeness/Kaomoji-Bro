extern crate reqwest;

use self::reqwest::header;
use std::collections::HashMap;
use std::fmt;

#[derive(Deserialize)]
pub struct Gateway {
    pub url: String,
    pub shards: i32,
}

impl fmt::Display for Gateway {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Gateway: {} w/ {} shards", self.url, self.shards)
    }
}

pub struct Client {
    pub client: reqwest::Client,
}

impl Client {
    pub fn new(config: &HashMap<String, String>) -> Result<Client, reqwest::Error> {
        let mut headers = header::Headers::new();
        headers.set(header::Authorization(format!(
            "Bot {}",
            config["bot-token"]
        )));
        let client: reqwest::Client = reqwest::Client::builder().default_headers(headers).build()?;
        Ok(Client { client })
    }

    pub fn get_gateway(&self, gateway_url: &String) -> Result<Gateway, reqwest::Error> {
        self.client.get(&gateway_url[..]).send()?.json()
    }
}
