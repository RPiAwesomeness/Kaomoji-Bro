extern crate config;
extern crate openssl;
extern crate reqwest;
extern crate serde_json;
extern crate url;
extern crate ws;

pub mod message;
pub mod payloads;

use super::http;
use serde_json::{from_reader, Value};
use std::collections::HashMap;
use std::env::current_dir;
use std::fs::File;
use std::sync::{atomic::AtomicBool, Arc};
use std::thread;
use std::time::Duration;

pub struct Client<'client> {
    http_client: &'client http::Client,
    out: Arc<ws::Sender>,
    heartbeat_wait: AtomicBool,
    session_id: Option<String>,
    config: &'client HashMap<String, String>,
    kaomoji: &'client HashMap<String, String>,
}

#[derive(Serialize, Deserialize)]
pub struct BasePayload {
    pub op: i32,
}

impl<'client> Client<'client> {
    fn identify(&self) -> ws::Result<()> {
        info!("Authenticating");

        let ident_data = payloads::IdentifyData {
            token: self.config.get("bot-token").unwrap().to_string(),
            properties: payloads::IdentifyProperties {
                os: "linux".to_string(),
                browser: "kaomoji-bro".to_string(),
                device: "kaomoji-bro".to_string(),
            },
            compress: false,
        };

        let ident = payloads::IdentifyPayload {
            op: 2,
            data: ident_data,
        };

        let ident_msg = ws::Message::from(serde_json::to_string(&ident).unwrap());

        self.out.send(ident_msg)
    }

    fn heartbeat(&self, period: u64, sequence: Option<i32>) {
        let out_clone = Arc::clone(&self.out);

        thread::spawn(move || {
            loop {
                // Heartbeat \o/
                info!("Lub dub!");

                let beat = payloads::Heartbeat { op: 1, d: sequence };

                let beat_msg = ws::Message::from(serde_json::to_string(&beat).unwrap());

                // Send the hearbeat payload to gateway
                let beat_result = out_clone.send(beat_msg);

                if let Err(err) = beat_result {
                    error!("Error occured in sending heartbeat!");
                    error!("Error: {:?}", err);
                }

                thread::sleep(Duration::from_millis(period));
            }
        });
    }

    fn send_message(
        &self,
        content: &str,
        channel_id: &str,
    ) -> Result<payloads::MessageData, reqwest::Error> {
        let url: &str = &format!(
            "https://discordapp.com/api/v6/channels/{}/messages",
            channel_id
        );
        let data = payloads::MessageSendData {
            content: content.to_string(),
        };

        self.http_client.client.post(url).json(&data).send()?.json()
    }
}

impl<'client> ws::Handler for Client<'client> {
    fn on_open(&mut self, handshake: ws::Handshake) -> ws::Result<()> {
        info!(
            "Connected to server at {}!",
            handshake.remote_addr().unwrap().unwrap()
        );

        Ok(())
    }

    fn on_message(&mut self, msg: ws::Message) -> ws::Result<()> {
        info!("Raw {}", msg);

        // Keep track of the channel ID the message was sent from, if any
        let mut channel_id: Option<String> = None;

        // Attempt to extract
        let base = serde_json::from_str::<BasePayload>(&msg.as_text().unwrap());
        let base = match base {
            Ok(base) => base,
            Err(err) => {
                error!("Failed to extract opcode from packet!");
                error!("Error: {}", err);
                return Ok(());
            }
        };

        let matched: Option<Box<payloads::Payload>> = match base.op {
            10 => {
                // Op-code 10 => Gateway Hello
                let payload = serde_json::from_str::<payloads::GatewayHelloPayload>(&msg.as_text().unwrap())
                    .unwrap();
                info!("Hearbeat Interval: {}", payload.d.interval);

                let ident_result = self.identify();

                if let Err(err) = &ident_result {
                    error!("Error sending opcode 2 Identify!");
                    error!("Message: {}", err);
                }

                Some(Box::new(payload))
            }
            0 => {
                let payload =
                    serde_json::from_str::<payloads::GenericOp>(&msg.as_text().unwrap()).unwrap();

                let payload: Option<Box<payloads::Payload>> =
                    match payload.op_type.to_lowercase().as_ref() {
                        "ready" => {
                            info!("Gateway ready!");

                            // Load READY op data
                            let payload = serde_json::from_str::<payloads::ReadyOpPayload>(
                                &msg.as_text().unwrap(),
                            ).unwrap();

                            self.session_id = Option::from(payload.data.session_id.clone());

                            let user_info: String = format!(
                                "{}:{} [ID: {}]",
                                payload.data.user.username,
                                payload.data.user.discriminator,
                                payload.data.user.id
                            );

                            info!("Got user {}", user_info);

                            Some(Box::new(payload))
                        }
                        "message_create" => {
                            // Handle message being sent
                            info!("Got a message");

                            let payload = serde_json::from_str::<payloads::MessageCreatePayload>(
                                &msg.as_text().unwrap(),
                            ).unwrap();

                            // Update the channel ID
                            channel_id = Some(payload.data.channel_id.clone());

                            message::parse_msg(payload.data, &self.kaomoji)
                        }
                        _ => {
                            error!("Unexpected op type {}", payload.op_type);
                            None
                        }
                    };

                payload
            }
            _ => {
                error!("Unexepected op {}", base.op);

                None
            }
        };

        if let Some(packet) = matched {
            // Handle the Gateway Hello and start the hearbeat
            if packet.opcode() == 10 {
                let packet_data: &payloads::GatewayHelloPayload = match packet
                    .as_any()
                    .downcast_ref::<payloads::GatewayHelloPayload>()
                {
                    Some(p) => p,
                    None => panic!("Unable to downcast packet to GatewayHelloPayload!"),
                };

                self.heartbeat(packet_data.d.interval, None);
            }

            // Handle sending the message
            if packet.type_name() == "message:send" {
                let packet_data = packet.as_any().downcast_ref::<payloads::MessageSendData>();

                // Successful downcast
                if let Some(p) = &packet_data {
                    if let Some(chan_id) = channel_id {
                        info!("Sending message {}", p.content);
                        if let Err(err) = self.send_message(&p.content, chan_id.as_ref()) {
                            error!("Failed to send message!");
                            error!("Error: {}", err);
                        }
                    } else {
                        error!("No channel ID set!");
                    }
                } else {
                    // Failed to downcast, log it and move on
                    error!("Failed to convert message data for send!");
                }
            }
        }

        Ok(())
    }
}

pub fn connect(host: &str, config: &HashMap<String, String>, client: &http::Client) {
    info!("Connecting to {}", host);

    // Load Kaomoji Hashmap
    let cur_path = current_dir().unwrap().join("kaomoji.json");
    let json = File::open(cur_path.as_path()).unwrap();

    let kaomoji: HashMap<String, String> = serde_json::from_reader(json).unwrap();

    ws::connect(host, |out| Client {
        http_client: client,
        out: Arc::new(out),
        heartbeat_wait: AtomicBool::new(false),
        session_id: None,
        config: config,
        kaomoji: &kaomoji,
    }).unwrap()
}
