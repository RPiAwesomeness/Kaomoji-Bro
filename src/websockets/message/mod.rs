use super::payloads;
use std::collections::HashMap;

pub fn parse_msg(
    data: payloads::MessageData,
    kaomoji: &HashMap<String, String>,
) -> Option<Box<payloads::Payload>> {
    info!(
        "[{}] Received {} from {}:{}",
        data.channel_id, data.content, data.author.username, data.author.discriminator,
    );

    // Check if it's a command
    if data.content.starts_with('!') {
        let kao: String = data.content.chars().skip(1).collect();
        let moji = match kaomoji.get(&kao) {
            Some(moji) => Option::from((*moji).as_str().to_string()),
            _ => None,
        };

        info!("Kaomoji for \"{}\": {:?}", kao, moji);

        if let Some(response) = moji {
            return Some(Box::new(payloads::MessageSendData { content: response }));
        }
    }

    None
}
