use std::any::Any;

pub trait Payload {
    fn opcode(&self) -> i32;
    fn type_name(&self) -> &str;
    fn as_any(&self) -> &Any;
}

// Unknown Op
pub struct UnknownOp {
    pub op: i32,
    pub type_name: String,
}

impl Payload for UnknownOp {
    fn opcode(&self) -> i32 {
        self.op
    }

    fn type_name(&self) -> &str {
        &self.type_name
    }

    fn as_any(&self) -> &Any {
        self
    }
}

// Generic Op
#[derive(Deserialize)]
pub struct GenericOp {
    pub op: i32,
    #[serde(rename = "s")]
    pub sequence: i32,
    #[serde(rename = "t")]
    pub op_type: String,
}

impl Payload for GenericOp {
    fn opcode(&self) -> i32 {
        self.op
    }

    fn type_name(&self) -> &str {
        "Generic 0 code op"
    }

    fn as_any(&self) -> &Any {
        self
    }
}

// Ready Op Payload
#[derive(Deserialize)]
pub struct ReadyOpPayload {
    pub op: i32,
    #[serde(rename = "s")]
    pub sequence: i32,
    #[serde(rename = "t")]
    pub op_type: String,
    #[serde(rename = "d")]
    pub data: ReadyData,
}

impl Payload for ReadyOpPayload {
    fn opcode(&self) -> i32 {
        self.op
    }

    fn type_name(&self) -> &str {
        self.op_type.as_ref()
    }

    fn as_any(&self) -> &Any {
        self
    }
}

#[derive(Deserialize)]
pub struct ReadyData {
    pub v: u32,
    pub user: User,
    pub session_id: String,
}

#[derive(Deserialize, Debug)]
pub struct User {
    pub id: String,
    pub username: String,
    pub discriminator: String,
    pub avatar: Option<String>,
    pub bot: Option<bool>,
    pub email: Option<String>,
}

// Gateway Hello Op Payload
#[derive(Deserialize)]
pub struct GatewayHelloPayload {
    pub op: i32,
    pub d: GatewayHello,
    pub s: Option<i32>,
    pub t: Option<String>,
    #[serde(default = "gateway_default")]
    pub type_name: String,
}

impl Payload for GatewayHelloPayload {
    fn opcode(&self) -> i32 {
        self.op
    }

    fn type_name(&self) -> &str {
        &self.type_name
    }

    fn as_any(&self) -> &Any {
        self
    }
}

fn gateway_default() -> String {
    "Gateway Hello".to_string()
}

// Message Event Payload
#[derive(Deserialize)]
pub struct MessageCreatePayload {
    pub op: i32,
    #[serde(rename = "s")]
    pub sequence: i32,
    #[serde(rename = "t")]
    pub op_type: String,
    #[serde(rename = "d")]
    pub data: MessageData,
}

impl Payload for MessageCreatePayload {
    fn opcode(&self) -> i32 {
        self.op
    }

    fn type_name(&self) -> &str {
        "message"
    }

    fn as_any(&self) -> &Any {
        self
    }
}

#[derive(Deserialize, Debug)]
pub struct MessageData {
    pub id: String,
    pub channel_id: String,
    pub author: User,
    pub content: String,
    pub timestamp: String,
    pub tts: bool,
    pub mention_everyone: bool,
    #[serde(rename = "type")]
    pub message_type: i32,
}

// Message Event Payload

#[derive(Serialize)]
pub struct MessageSendData {
    pub content: String,
}

impl Payload for MessageSendData {
    fn opcode(&self) -> i32 {
        0
    }

    fn type_name(&self) -> &str {
        "message:send"
    }

    fn as_any(&self) -> &Any {
        self
    }
}

// Gateway Hello
#[derive(Deserialize)]
pub struct GatewayHello {
    #[serde(rename = "heartbeat_interval")]
    pub interval: u64,
}

// Heartbeat
#[derive(Serialize)]
pub struct Heartbeat {
    pub op: i32,
    pub d: Option<i32>,
}

// Identify Payload
#[derive(Serialize, Debug)]
pub struct IdentifyPayload {
    pub op: i32,
    #[serde(rename = "d")]
    pub data: IdentifyData,
}

#[derive(Serialize, Debug)]
pub struct IdentifyData {
    pub token: String,
    pub properties: IdentifyProperties,
    pub compress: bool,
}

#[derive(Serialize, Debug)]
pub struct IdentifyProperties {
    #[serde(rename = "$os")]
    pub os: String,
    #[serde(rename = "$browser")]
    pub browser: String,
    #[serde(rename = "$device")]
    pub device: String,
}
