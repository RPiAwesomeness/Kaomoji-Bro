#[macro_use]
extern crate log;
#[macro_use]
extern crate serde_derive;
extern crate config;
extern crate serde;
extern crate serde_json;
extern crate simple_logger;

mod http;
mod websockets;

use std::collections::HashMap;

const BASE_URL: &'static str = "https://discordapp.com/api/v6";
const GATEWAY: &'static str = "/gateway/bot";

fn load_config() -> HashMap<String, String> {
    let mut settings = config::Config::default();

    info!("Loading config");

    // Add YAML config file
    settings
        .merge(config::File::with_name("config.yaml"))
        .unwrap();

    settings.try_into::<HashMap<String, String>>().unwrap()
}

fn main() {
    simple_logger::init_with_level(log::Level::Info).unwrap();

    // Load config
    let config = load_config();
    info!("Config loaded!");

    info!("Y'all ready to drop some memes? (☞ ﾟヮﾟ)☞");

    // Get Gateway
    let gateway_url: String = BASE_URL.to_string() + &GATEWAY.to_string();
    let client: http::Client = match http::Client::new(&config) {
        Ok(c) => c,
        Err(err) => panic!("Unable to create client! {}", err),
    };

    let body = client.get_gateway(&gateway_url);

    if let Err(err) = &body {
        error!("Failed to get gateway info!");
        error!("Error: {}", err);
        warn!("Aborting execution!");
        return;
    }

    // Get the gateway from the response
    let gateway = body.unwrap();

    websockets::connect(&gateway.url, &config, &client);
}
